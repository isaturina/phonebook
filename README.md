**AddressBook application**



This is the simple example of Address Book. It contains 2 views - one for address list and second for address details(including the list of phone numbers). 


The application allows to perform several operations to phone numbers and addresses: 

 - View list of addresses

 - Create new address entry

 - Delete address

 - View list of phone numbers attached to the address

 - Add phone number to address entry

 - Delete phone number

All data is stored in Postgresql database. 