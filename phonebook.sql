--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.14
-- Dumped by pg_dump version 9.1.14
-- Started on 2014-11-18 23:50:15 EET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1910 (class 1262 OID 23348)
-- Name: phonebook; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE phonebook WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE phonebook OWNER TO postgres;

\connect phonebook

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 165 (class 3079 OID 11680)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1913 (class 0 OID 0)
-- Dependencies: 165
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 161 (class 1259 OID 23349)
-- Dependencies: 5
-- Name: address; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE address (
    country character varying,
    address_line character varying,
    city character varying,
    address_id integer NOT NULL
);


ALTER TABLE public.address OWNER TO postgres;

--
-- TOC entry 163 (class 1259 OID 23380)
-- Dependencies: 161 5
-- Name: address_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE address_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_address_id_seq OWNER TO postgres;

--
-- TOC entry 1914 (class 0 OID 0)
-- Dependencies: 163
-- Name: address_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE address_address_id_seq OWNED BY address.address_id;


--
-- TOC entry 162 (class 1259 OID 23363)
-- Dependencies: 5
-- Name: phonenumber; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE phonenumber (
    type character varying,
    country_code character varying,
    number character varying,
    address bigint,
    phonenumber_id integer NOT NULL
);


ALTER TABLE public.phonenumber OWNER TO postgres;

--
-- TOC entry 164 (class 1259 OID 23389)
-- Dependencies: 5 162
-- Name: phonenumber_phonenumber_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE phonenumber_phonenumber_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phonenumber_phonenumber_id_seq OWNER TO postgres;

--
-- TOC entry 1915 (class 0 OID 0)
-- Dependencies: 164
-- Name: phonenumber_phonenumber_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE phonenumber_phonenumber_id_seq OWNED BY phonenumber.phonenumber_id;


--
-- TOC entry 1795 (class 2604 OID 23382)
-- Dependencies: 163 161
-- Name: address_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address ALTER COLUMN address_id SET DEFAULT nextval('address_address_id_seq'::regclass);


--
-- TOC entry 1796 (class 2604 OID 23391)
-- Dependencies: 164 162
-- Name: phonenumber_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY phonenumber ALTER COLUMN phonenumber_id SET DEFAULT nextval('phonenumber_phonenumber_id_seq'::regclass);


--
-- TOC entry 1902 (class 0 OID 23349)
-- Dependencies: 161 1906
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY address (country, address_line, city, address_id) FROM stdin;
Ukraine	Somestreet 1	Dnepropetrovsk	1
Ukraine	du ddd	Kiev	2
\.


--
-- TOC entry 1916 (class 0 OID 0)
-- Dependencies: 163
-- Name: address_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('address_address_id_seq', 92, true);


--
-- TOC entry 1903 (class 0 OID 23363)
-- Dependencies: 162 1906
-- Data for Name: phonenumber; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY phonenumber (type, country_code, number, address, phonenumber_id) FROM stdin;
mobile	+38	0991222111	1	1
mobile	+38	0991222112	1	2
mobile	+38	0991222113	2	3
\.


--
-- TOC entry 1917 (class 0 OID 0)
-- Dependencies: 164
-- Name: phonenumber_phonenumber_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('phonenumber_phonenumber_id_seq', 55, true);


--
-- TOC entry 1798 (class 2606 OID 23399)
-- Dependencies: 161 161 1907
-- Name: address_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_id PRIMARY KEY (address_id);


--
-- TOC entry 1800 (class 2606 OID 23401)
-- Dependencies: 162 162 1907
-- Name: phonenumber_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY phonenumber
    ADD CONSTRAINT phonenumber_id PRIMARY KEY (phonenumber_id);


--
-- TOC entry 1912 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-11-18 23:50:15 EET

--
-- PostgreSQL database dump complete
--

