package com.saturina.phonebook.controllers;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.saturina.phonebook.dao.DAOFactory;
import com.saturina.phonebook.model.Address;
import com.saturina.phonebook.model.PhoneNumber;

@Controller
public class ShowAddressController extends GenericController {
	@RequestMapping("/show")
	public String show(@RequestParam("id") Long id, Model model) {
		Address address = getAddressDAO(DAOFactory.POSTGRESQL).find(id);
		model.addAttribute("address", address);
		List<PhoneNumber> numbers = getPhoneNumberDAO(DAOFactory.POSTGRESQL)
				.findAllByAddressId(id);
		model.addAttribute("numbers", numbers);
		return "show";
	}

	@RequestMapping("/deleteNumber")
	public String delete(@RequestParam("addressId") Long addressId,
			@RequestParam("numberId") Long numberId) {
		getPhoneNumberDAO(DAOFactory.POSTGRESQL).delete(numberId);
		return "redirect:/show?id=" + addressId;
	}

	@RequestMapping("/add")
	public String create(@RequestParam("addressId") String addressId,
			@RequestParam("type") String type,
			@RequestParam("countryCode") String countryCode,
			@RequestParam("number") String number) {
		PhoneNumber pn = new PhoneNumber();
		pn.setType(type);
		pn.setCountryCode(countryCode);
		pn.setNumber(number);
		Address address = getAddressDAO(DAOFactory.POSTGRESQL).find(
				Long.valueOf(addressId));

		pn.setAddress(address);
		getPhoneNumberDAO(DAOFactory.POSTGRESQL).insert(pn);
		return "redirect:/show?id=" + addressId;
	}

}