package com.saturina.phonebook.controllers;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.saturina.phonebook.dao.DAOFactory;
import com.saturina.phonebook.model.Address;

@Controller
public class ListController extends GenericController {

	@RequestMapping("/list")
	public String list(Model model) {
		List<Address> list = getAddressDAO(DAOFactory.POSTGRESQL).findAll();
		model.addAttribute("list", list);
		return "list";
	}

	@RequestMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		getAddressDAO(DAOFactory.POSTGRESQL).delete(id);
		return "redirect:/list";
	}
	@RequestMapping("/create")
	public String create(@RequestParam("city") String city, @RequestParam("country") String country, @RequestParam("address") String address) {
		Address addr = new Address();
		addr.setAddressLine(address);
		addr.setCity(city);
		addr.setCountry(country);
		getAddressDAO(DAOFactory.POSTGRESQL).insert(addr);
		return "redirect:/list";
	}
}