package com.saturina.phonebook.controllers;

import com.saturina.phonebook.dao.AddressDAO;
import com.saturina.phonebook.dao.DAOFactory;
import com.saturina.phonebook.dao.PhoneNumberDAO;

public class GenericController {

	public AddressDAO getAddressDAO(int type) {
		DAOFactory pgFactory = DAOFactory.getDAOFactory(type);
		return pgFactory.getAddressDAO();
	}
	
	public PhoneNumberDAO getPhoneNumberDAO(int type) {
		DAOFactory pgFactory = DAOFactory.getDAOFactory(type);
		return pgFactory.getPhoneNumberDAO();
	}
}
