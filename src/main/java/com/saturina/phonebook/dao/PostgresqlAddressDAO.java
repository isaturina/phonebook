package com.saturina.phonebook.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.saturina.phonebook.model.Address;
import com.saturina.phonebook.model.PhoneNumber;

public class PostgresqlAddressDAO implements AddressDAO {

	private Connection connection;

	public Connection getConnection() {
		return connection;
	}

	public PostgresqlAddressDAO(Connection connection) {
		super();
		this.connection = connection;
	}

	public long insert(Address address) {
		Long addressId = null;
		PhoneNumberDAO phoneNumberDAO = new PostgresqlPhoneNumberDAO(connection);
		try {
			PreparedStatement pstmt = connection
					.prepareStatement(
							"INSERT INTO address (address_line, city, country) VALUES (?,?,?)",
							Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, address.getAddressLine());
			pstmt.setString(2, address.getCity());
			pstmt.setString(3, address.getCountry());

			int affectedRows = pstmt.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException(
						"Creating address failed, no rows affected.");
			}

			try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					addressId = generatedKeys.getLong("address_id");
				} else {
					throw new SQLException(
							"Creating address failed, no ID obtained.");
				}
			}

			if (address.getPhoneNumbers() != null
					&& !address.getPhoneNumbers().isEmpty()) {
				for (PhoneNumber phoneNumber : address.getPhoneNumbers()) {
					phoneNumber.setAddress(find(addressId));
					phoneNumberDAO.insert(phoneNumber);
				}
			}

			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return addressId;
	}

	public boolean delete(Long addressId) {
		try {
			PreparedStatement pstmt = connection
					.prepareStatement("DELETE FROM address WHERE address_id=?");
			pstmt.setLong(1, addressId);
			pstmt.executeUpdate();

			pstmt.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public Address find(Long addressId) {
		Address address = new Address();
		Statement st;
		try {
			st = connection.createStatement();
			ResultSet rs = st
					.executeQuery("SELECT * FROM address WHERE address_id="
							+ addressId);

			if (rs.next()) {
				address.setAddressId(rs.getLong("address_id"));
				address.setAddressLine(rs.getString("address_line"));
				address.setCountry(rs.getString("country"));
				address.setCity(rs.getString("city"));
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return address;

	}

	public boolean update(Address address) {
		PhoneNumberDAO phoneNumberDAO = new PostgresqlPhoneNumberDAO(connection);
		try {
			PreparedStatement pstmt = connection
					.prepareStatement("UPDATE address SET address_line=?, city=?, country=? WHERE address_id=?");
			pstmt.setString(1, address.getAddressLine());
			pstmt.setString(2, address.getCity());
			pstmt.setString(3, address.getCountry());
			pstmt.setLong(4, address.getAddressId());
			pstmt.executeUpdate();
			
			if (address.getPhoneNumbers() != null
					&& !address.getPhoneNumbers().isEmpty()) {
				for (PhoneNumber phoneNumber : address.getPhoneNumbers()) {
					phoneNumber.setAddress(find(address.getAddressId()));
					phoneNumberDAO.update(phoneNumber);
				}
			}

			pstmt.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<Address> findAll() {
		List<Address> list = new ArrayList<Address>();
		Statement st;
		try {
			st = connection.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM address");
			while (rs.next()) {
				Address address = new Address();
				address.setAddressId(rs.getLong("address_id"));
				address.setAddressLine(rs.getString("address_line"));
				address.setCountry(rs.getString("country"));
				address.setCity(rs.getString("city"));
				list.add(address);
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}

}
