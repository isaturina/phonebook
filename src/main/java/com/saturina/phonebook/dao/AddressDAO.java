package com.saturina.phonebook.dao;

import java.sql.Connection;
import java.util.List;

import com.saturina.phonebook.model.Address;

public interface AddressDAO {
	public Connection getConnection();

	public long insert(Address address);

	public boolean delete(Long addressId);

	public Address find(Long addressId);

	public List<Address> findAll();

	public boolean update(Address address);
}
