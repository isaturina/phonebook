package com.saturina.phonebook.dao;

public abstract class DAOFactory {
	public static final int POSTGRESQL = 1;

	public abstract AddressDAO getAddressDAO();
	public abstract PhoneNumberDAO getPhoneNumberDAO();

	public static DAOFactory getDAOFactory(int whichFactory) {

		switch (whichFactory) {
		case POSTGRESQL:
			return new PostgresqlDAOFactory();
		default:
			return null;
		}
	}
}
