package com.saturina.phonebook.dao;

import java.sql.Connection;
import java.util.List;

import com.saturina.phonebook.model.PhoneNumber;

public interface PhoneNumberDAO {
	public Connection getConnection();

	public long insert(PhoneNumber phoneNumber);

	public boolean delete(Long phoneNumberId);

	public PhoneNumber find(Long phoneNumberId);

	public boolean update(PhoneNumber phoneNumber);

	public List<PhoneNumber> findAllByAddressId(long addressId);
}
