package com.saturina.phonebook.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class PostgresqlDAOFactory extends DAOFactory {
	public static final String DRIVER = "org.postgresql.Driver";
	private Connection connection;

	public PostgresqlDAOFactory() {
		super();
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/phonebook", "postgres",
					"postgres");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public AddressDAO getAddressDAO() {
		return new PostgresqlAddressDAO(connection);
	}

	@Override
	public PhoneNumberDAO getPhoneNumberDAO() {
		return new PostgresqlPhoneNumberDAO(connection);
	}

}
