package com.saturina.phonebook.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.saturina.phonebook.model.Address;
import com.saturina.phonebook.model.PhoneNumber;

public class PostgresqlPhoneNumberDAO implements PhoneNumberDAO {

	private Connection connection;

	public long insert(PhoneNumber phoneNumber) {
		Long phoneNumberId = null;
		try {
			String sql;
			if(phoneNumber.getAddress() != null) {
				sql = "INSERT INTO phonenumber (country_code, type, number, address) VALUES (?,?,?,?)";
			} else {
				sql = "INSERT INTO phonenumber (country_code, type, number) VALUES (?,?,?)";
			}
			PreparedStatement pstmt = connection
					.prepareStatement(
							sql,
							Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, phoneNumber.getCountryCode());
			pstmt.setString(2, phoneNumber.getType());
			pstmt.setString(3, phoneNumber.getNumber());
			
			if(phoneNumber.getAddress() != null) {
				pstmt.setLong(4, phoneNumber.getAddress().getAddressId());
			} 
			int affectedRows = pstmt.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException(
						"Creating phoneNumber failed, no rows affected.");
			}

			try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					phoneNumberId = generatedKeys.getLong("phonenumber_id");
				} else {
					throw new SQLException(
							"Creating phoneNumber failed, no ID obtained.");
				}
			}

			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return phoneNumberId;
	}

	public PostgresqlPhoneNumberDAO(Connection connection) {
		super();
		this.connection = connection;
	}

	public boolean delete(Long phoneNumberId) {
		try {
			PreparedStatement pstmt = connection
					.prepareStatement("DELETE FROM phonenumber WHERE phonenumber_id=?");
			pstmt.setLong(1, phoneNumberId);
			pstmt.executeUpdate();

			pstmt.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public PhoneNumber find(Long phoneNumberId) {
		PhoneNumber phoneNumber = new PhoneNumber();
		Statement st;
		try {
			st = connection.createStatement();
			ResultSet rs = st
					.executeQuery("SELECT * FROM phonenumber WHERE phonenumber_id="
							+ phoneNumberId);

			if (rs.next()) {
				phoneNumber.setPhoneNumberId(rs.getLong("phonenumber_id"));
				phoneNumber.setCountryCode(rs.getString("country_code"));
				phoneNumber.setNumber(rs.getString("number"));
				phoneNumber.setType(rs.getString("type"));
				phoneNumber.setAddress((Address) rs.getObject("address"));
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return phoneNumber;
	}

	public boolean update(PhoneNumber phoneNumber) {
		try {
			PreparedStatement pstmt = connection
					.prepareStatement("UPDATE phonenumber SET country_code=?, number=?, type=? WHERE phonenumber_id=?");
			pstmt.setString(1, phoneNumber.getCountryCode());
			pstmt.setString(2, phoneNumber.getNumber());
			pstmt.setString(3, phoneNumber.getType());
			pstmt.setLong(4, phoneNumber.getPhoneNumberId());
			pstmt.executeUpdate();

			pstmt.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<PhoneNumber> findAllByAddressId(long addressId) {
		List<PhoneNumber> list = new ArrayList<PhoneNumber>();
		Statement st;
		try {
			st = connection.createStatement();
			ResultSet rs = st
					.executeQuery("SELECT * FROM phonenumber WHERE address="
							+ addressId);
			while (rs.next()) {
				PhoneNumber phoneNumber = new PhoneNumber();
				phoneNumber.setPhoneNumberId(rs.getLong("phonenumber_id"));
				phoneNumber.setCountryCode(rs.getString("country_code"));
				phoneNumber.setType(rs.getString("type"));
				phoneNumber.setNumber(rs.getString("number"));
				list.add(phoneNumber);
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}

	@Override
	public Connection getConnection() {
		return connection;
	}

}
