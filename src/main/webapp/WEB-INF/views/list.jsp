<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List</title>
</head>
<body>
	<table>
		<c:forEach var="address" items="${list}">
			<tr>
				<td><c:out value="${address.city}"></c:out></td>
				<td><c:out value="${address.country}"></c:out></td>
				<td><c:out value="${address.addressLine}"></c:out></td>
				<td><a
					href="<c:url value="show">  <c:param name="id" value="${address.addressId}"/> </c:url>">
						Show </a></td>
				<td><a
					href="<c:url value="delete">  <c:param name="id" value="${address.addressId}"/> </c:url>">
						Delete </a></td>
			</tr>
		</c:forEach>
	</table>
<div id="addForm" > </div>
	<input type="button" value="Add" onclick="addForm()" />

	<script language="javascript">
		function addForm() {
			 var ni = document.getElementById('addForm');
			 var newform = document.createElement('form');
			 newform.setAttribute('name','addForm');
			 newform.setAttribute('method','get');
			 newform.setAttribute('action','create');
				 newform.innerHTML = 'Country: <input type="text" name="country"><br/>' +
				 'City: <input type="text" name="city"><br/>Address: <input type="text" name="address"><br/>' + 
				 '<input type="submit" name="submit" value="Submit">';
			 
			 ni.appendChild(newform);

		}
	</script>
</body>
</html>