<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%
 String addressId=request.getParameter("id");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Address Item</title>
</head>
<body>
	<table>
		<tr>
			<td><c:out value="${address.city}"></c:out></td>
			<td><c:out value="${address.addressLine}"></c:out></td>
		</tr>
	</table>
	<table>
		<c:forEach var="phoneNumber" items="${numbers}">
			<tr>
				<td><c:out value="${phoneNumber.countryCode}"></c:out></td>
				<td><c:out value="${phoneNumber.number}"></c:out></td>
				<td><c:out value="${phoneNumber.type}"></c:out></td>
				<td><a
					href="<c:url value="deleteNumber">  <c:param name="numberId" value="${phoneNumber.phoneNumberId}"/> <c:param name="addressId" value="${address.addressId}"/> </c:url>">
						Delete </a></td>
			</tr>
		</c:forEach>
	</table>
	<div id="addForm"></div>
	<input type="button" value="Add Phone Number" onclick="addForm()" />
	<a href="<c:url value="list"></c:url>">
						Back to Address Book </a>
	<script language="javascript">
		function addForm() {
			var ni = document.getElementById('addForm');
			var newform = document.createElement('form');
			newform.setAttribute('name', 'addForm');
			newform.setAttribute('method', 'get');
			newform.setAttribute('action', 'add');
			newform.innerHTML = 'Type: <input type="text" name="type"><br/>'
					+ 'Country Code: <input type="text" name="countryCode"><br/>number: <input type="text" name="number"><br/>'
					+ '<input type="hidden" name="addressId" value="<%=addressId%>">' 
					+ '<input type="submit" name="submit" value="Submit">';

			ni.appendChild(newform);

		}
	</script>

</body>
</html>