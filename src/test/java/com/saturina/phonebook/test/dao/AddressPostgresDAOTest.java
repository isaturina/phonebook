package com.saturina.phonebook.test.dao;

import static org.junit.Assert.*;

import java.sql.Connection;

import org.junit.Test;

import com.saturina.phonebook.dao.AddressDAO;
import com.saturina.phonebook.dao.DAOFactory;
import com.saturina.phonebook.model.Address;

public class AddressPostgresDAOTest {
	@Test
	public void testAddressDAO() throws Exception {

		AddressDAO addressDAO = DAOFactory.getDAOFactory(DAOFactory.POSTGRESQL)
				.getAddressDAO();

		Connection connection = addressDAO.getConnection();
		connection.setAutoCommit(false);
		
		try {

			Address address1 = new Address();
			address1.setAddressLine("testAddr");
			address1.setCity("TestCity");
			address1.setCountry("TestCountry");

			Address address2 = new Address();
			address2.setAddressLine("testAddr1");
			address2.setCity("TestCity1");
			address2.setCountry("TestCountry1");

			long addr1Id = addressDAO.insert(address1);
			long addr2Id = addressDAO.insert(address2);

			Address address1Saved = addressDAO.find(addr1Id);
			Address address2Saved = addressDAO.find(addr2Id);
			
			assertEquals(address1, address1Saved);
			assertEquals(address2, address2Saved);
			
			address1Saved.setAddressLine("addr1Upd");
			address2Saved.setAddressLine("addr2Upd");

			addressDAO.update(address1Saved);
			addressDAO.update(address2Saved);

			Address address1Updated = addressDAO.find(addr1Id);
			Address address2Updated = addressDAO.find(addr2Id);

			assertEquals("addr1Upd", address1Updated.getAddressLine());
			assertEquals("addr2Upd", address2Updated.getAddressLine());

			addressDAO.delete(addr1Id);
			addressDAO.delete(addr2Id);

			assertNull(addressDAO.find(addr1Id).getAddressId());
			assertNull(addressDAO.find(addr2Id).getAddressId());

		} finally {
			connection.rollback();
			connection.close();
		}
	}
}
