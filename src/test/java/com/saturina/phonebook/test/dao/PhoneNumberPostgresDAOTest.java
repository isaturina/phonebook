package com.saturina.phonebook.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.sql.Connection;

import org.junit.Test;

import com.saturina.phonebook.dao.DAOFactory;
import com.saturina.phonebook.dao.PhoneNumberDAO;
import com.saturina.phonebook.model.PhoneNumber;

public class PhoneNumberPostgresDAOTest {
	@Test
	public void testAddressDAO() throws Exception {

		PhoneNumberDAO phoneNumberDAO = DAOFactory.getDAOFactory(DAOFactory.POSTGRESQL)
				.getPhoneNumberDAO();

		Connection connection = phoneNumberDAO.getConnection();
		connection.setAutoCommit(false);
		
		try {

			PhoneNumber phoneNumber1 = new PhoneNumber();
			phoneNumber1.setCountryCode("countryCode");
			phoneNumber1.setNumber("112112");
			phoneNumber1.setType("type");

			PhoneNumber phoneNumber2 = new PhoneNumber();
			phoneNumber2.setCountryCode("countryCode1");
			phoneNumber2.setNumber("1121123");
			phoneNumber2.setType("type1");

			long pn1Id = phoneNumberDAO.insert(phoneNumber1);
			long pn2Id = phoneNumberDAO.insert(phoneNumber2);

			PhoneNumber phoneNumber1Saved = phoneNumberDAO.find(pn1Id);
			PhoneNumber phoneNumber2Saved = phoneNumberDAO.find(pn2Id);
			
			assertEquals(phoneNumber1, phoneNumber1Saved);
			assertEquals(phoneNumber2, phoneNumber2Saved);
			
			phoneNumber1Saved.setType("type1Upd");
			phoneNumber2Saved.setType("type2Upd");

			phoneNumberDAO.update(phoneNumber1Saved);
			phoneNumberDAO.update(phoneNumber2Saved);

			PhoneNumber phoneNumber1Updated = phoneNumberDAO.find(pn1Id);
			PhoneNumber phoneNumber2Updated = phoneNumberDAO.find(pn2Id);

			assertEquals("type1Upd", phoneNumber1Updated.getType());
			assertEquals("type2Upd", phoneNumber2Updated.getType());

			phoneNumberDAO.delete(pn1Id);
			phoneNumberDAO.delete(pn2Id);

			assertNull(phoneNumberDAO.find(pn1Id).getPhoneNumberId());
			assertNull(phoneNumberDAO.find(pn2Id).getPhoneNumberId());

		} finally {
			connection.rollback();
			connection.close();
		}
	}
}
